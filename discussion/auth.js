const jwt = require('jsonwebtoken');

//
const secret = "CourseBookingAPI ";

//
module.exports.createAccessToken = (user) => {
   // the data will be received from the 
   //registration form when the user logs in, a token will be create with user's info
    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    };
    //this generates a JSON wen token using the jwt.sign() method
    // Generates the token using the form data and secret code with no 
    //additional options provided
    return jwt.sign(data, secret, {})
}