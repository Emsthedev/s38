const User = require('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth')

//checkEmailExists
module.exports.checkEmailExists = (reqBody) => {
    return User.find({
        email: reqBody.email
    })
    .then(result => {
        if (result.length > 0){
            return true
        } else{
            return false
        }
    })
};

//registerUser

module.exports.registerUser = (reqBody) => {
    let newUser = new User({
        firstName : reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        mobileNo: reqBody.mobileNo,
        password: bcrypt.hashSync(reqBody.password, 10)
    })
    return newUser.save().then((user, error) =>{
        if(error){
            return false
        } else {
            return true
        }
    })
}

//loginUser
module.exports.loginUser = (reqBody) => {
    return User.findOne({
        email: reqBody.email
    })
    .then(result => {
        if (result == null){
            return false
        } else{
            //compareSync(<data to compare>, <encrypted password>)
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
            if(isPasswordCorrect){
                return {access: auth.createAccessToken(result)}
            } else{
                return false
            }
        }
    })
};


    module.exports.getProfile = (reqBody) => {
        return User.findById(reqBody.id)
        .then(result => {
            if(result == null) {
              return false
            }else {
            result.password="******";
            return result
            }
        })
    };
    


 
    