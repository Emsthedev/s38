const mongoose = require ('mongoose');

//creating schema

const courseSchema = new mongoose.Schema({
        name: {
            type: String,
            required: [true, "Course Name is required"]
        },
        description: {
            type: String,
            required: [true, "Description is required"]
        },
        isActive: {
            type: Boolean,
            default: true
        },
        price: {
            type: Number,
            required: [true, "Price is required"]
        },
        createdOn: {
            type: Date,
            default: new Date()
        },
        enrolles: [{
            userId: {
                type: String,
                require: [true, "User ID is reuired"]
            },
            enrolledOn: {
                type: Date,
                default: new Date()
            }
        }]


});


module.exports = mongoose.model("Course", courseSchema)