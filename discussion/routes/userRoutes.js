const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');

//Creating Routes

//checkEmail routes - checks if email is exisiting in db
router.post('/checkEmail', (req,res)=>{
    userController.checkEmailExists(req.body)
    .then(resultFromController => res.send(resultFromController))
});

//register route - create user in db
router.post ("/register", (req,res) => {
    userController.registerUser(req.body)
    .then(resultFromController => 
        res.send(resultFromController))
});

//auth
router.post("/login", (req, res)=>{
    userController.loginUser(req.body)
    .then(resultFromController => 
        res.send(resultFromController))
})
//userdetail
router.post("/details", (req, res)=>{
    userController.getProfile(req.body)
    .then(resultFromController => 
        res.send(resultFromController))
})



module.exports = router;
